/**
 * Copyright (C) 2010-2015 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.orgin/licenses/agpl-3.0.html>.
 **/





package net.biomodels.jummp.indexing
/**
 * Simple ModelIndexer implementation containing methods common to all concrete subclasses.
 *
 * @author raza
 * @author Mihai Glonț <mihai.glont@ebi.ac.uk>
 * @author Tung Nguyen <tnguyen@ebi.ac.uk>
 */
abstract class AbstractModelIndexer implements ModelIndexer {
    /**
     * Concrete model indexing strategy implementation.
     */
    IndexingStrategy indexingStrategy

    IndexDataContext indexDataContext = new IndexDataContext()
    /**
     * Persists the supplied data in a Solr core.
     *
     * @param data a mapping between fields and corresponding values that should be indexed.
     * @param solrURL the URL that can be used to access the Solr core where the data should
     * be sent.
     */
    void indexData(RequestContext ctx) {
        indexDataContext.setProcessingIndexedDataStrategy(ctx.searchStrategy)
        indexDataContext.indexDataBasedSearchStrategy(ctx)
    }
}

