/**
 * Copyright (C) 2010-2016 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.orgin/licenses/agpl-3.0.html>.
 **/





package net.biomodels.jummp.indexing.solrindexer

import net.biomodels.jummp.indexing.ProcessingIndexDataStrategy
import net.biomodels.jummp.indexing.RequestContext
import org.apache.solr.client.solrj.SolrClient
import org.apache.solr.client.solrj.impl.HttpSolrClient
import org.apache.solr.client.solrj.response.UpdateResponse
import org.apache.solr.common.SolrInputDocument
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * @author Mihai Glonț <mihai.glont@ebi.ac.uk>
 * @author Tung Nguyen <tnguyen@ebi.ac.uk>
 */

class ProcessingIndexDataSolr implements ProcessingIndexDataStrategy {
    /**
     * The class logger.
     */
    private static final Logger log = LoggerFactory.getLogger(this.getClass())
    private static final boolean IS_DEBUG_ENABLED = log.isDebugEnabled()
    private static final boolean IS_INFO_ENABLED = log.isInfoEnabled()
    private static final boolean IS_ERROR_ENABLED = log.isErrorEnabled()

    ProcessingIndexDataSolr() {
        if (IS_DEBUG_ENABLED) {
            log.debug("--- OmicsDI indexing")
        }
    }

    void runIndexData(RequestContext ctx) {
        if (IS_INFO_ENABLED) {
            log.info "Beginning to index data for Solr..."
        }
        Map<String, String> data = ctx.partialData
        String solrURL = ctx.solrServer
        SolrClient solrClient = new HttpSolrClient(solrURL)
        SolrInputDocument doc = new SolrInputDocument()
        data.each { entry, oneOrMoreValues ->
            if (oneOrMoreValues instanceof String) {
                if (entry == "submissionDate" || entry == "lastModified") {
                    def asDate = Date.parse("yyyy-MM-dd'T'HH:mm:ssZ", oneOrMoreValues)
                    doc.addField(entry, asDate)
                }
                else {
                    doc.addField(entry, oneOrMoreValues)
                }
            }
            else {
                oneOrMoreValues.each { singleValue ->
                    doc.addField(entry, singleValue)
                }
            }
        }
        solrClient.add(doc)
        if (IS_INFO_ENABLED) {
            log.info("Forcing commit of changes.")
        }
        UpdateResponse commitResponse = solrClient.commit()
        if (commitResponse.getStatus() == 500) {
            if (IS_ERROR_ENABLED) {
                log.error("Failed to commit the document to the Solr Server. Server returned status code ${commitResponse.getStatus()}")
            }
        } else {
            if (IS_INFO_ENABLED) {
                log.info("Commit successfully.")
            }
        }
        if (IS_INFO_ENABLED) {
            log.info "Ending to index data for Solr..."
        }
    }
}
