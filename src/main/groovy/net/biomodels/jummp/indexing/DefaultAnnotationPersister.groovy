/**
 * Copyright (C) 2010-2015 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.orgin/licenses/agpl-3.0.html>.
 **/

package net.biomodels.jummp.indexing

import groovy.transform.CompileStatic
import groovy.transform.TypeCheckingMode
import net.biomodels.jummp.annotationstore.ElementAnnotation
import net.biomodels.jummp.annotationstore.Qualifier
import net.biomodels.jummp.annotationstore.ResourceReference
import net.biomodels.jummp.annotationstore.RevisionAnnotation
import net.biomodels.jummp.annotationstore.Statement
import net.biomodels.jummp.model.ModelElementType
import net.biomodels.jummp.model.Revision
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.orm.hibernate4.HibernateOptimisticLockingFailureException

/**
 * Simple concrete strategy for saving annotations to the database.
 *
 * @author Raza Ali
 * @author Mihai Glonț <mihai.glont@ebi.ac.uk>
 */
@CompileStatic
class DefaultAnnotationPersister implements AnnotationPersister {
    /**
     * The class logger.
     */
    private static final Logger log = LoggerFactory.getLogger(DefaultAnnotationPersister.class)

    /**
     * Saves a given Statement to the database.
     *
     * @see net.biomodels.jummp.annotationstore.Statement
     * @param statement the statement to persist.
     * @param ctx the request's context.
     * @return the ElementAnnotation object corresponding to @p statement.
     */
    @Override
    @CompileStatic(TypeCheckingMode.SKIP)
    ElementAnnotation save(Statement statement, ModelElementType elementType, RequestContext ctx) {
        Map<String, Object> indexData = ctx.partialData
        String creator = indexData.submitter
        long revisionId = (long) indexData.revision_id

        def revision = Revision.get(revisionId)

        final String s = statement.subjectId
        final Qualifier qualifier = statement.qualifier
        final ResourceReference rr = statement.object
        // no Statement id <=> need to create an ElementAnnotation as well!
        ElementAnnotation annotation
        if (!statement.id) {
            annotation = new ElementAnnotation(statement: statement, creatorId: creator,
                    modelElementType: elementType)
        } else {
            // statement is already persisted and associated with at least 1 element annotation
            List<ElementAnnotation> statementAnnotations =
                    ElementAnnotation.findAllByStatement(statement)

            // domain model precondition
            if (!statementAnnotations) {
                log.error "No annotation found for {} -- {} {} {}. Refusing to create one for {}",
                        statement, s, qualifier.uri, rr.uri, revision
                return null
            }

            List<ElementAnnotation> statementElementAnnotations = statementAnnotations.findAll {
                it.modelElementType == elementType
            }
            if (!statementElementAnnotations) {
                // create annotation for statement and element type
                annotation = new ElementAnnotation(statement: statement, creatorId: creator,
                       modelElementType: elementType)
            } else {
                // check there isn't another element annotation with same revision and statement
                for (ElementAnnotation ea : statementElementAnnotations) {
                    if (RevisionAnnotation.exists(revisionId, ea.id)) {
                        log.error "There is already an ElementAnnotation for {} and {}", statement,
                            revision
                        return null
                    }
                }
                // now we can reuse an existing annotation with the same statement and element type
                annotation = statementElementAnnotations.first()   // there should only be 1
            }

            if (!statement.validate()) {
                final String uid = indexData.uniqueId
                final String q = qualifier?.accession
                final String r = rr?.accession
                log.error """\
    Cannot persist statement $s $q $r for revision ${uid} -- ${statement.errors.allErrors}"""
                return null
            }
        }
        try {
            if (!annotation.save()) {
                log.error "Cannot save annotation ${annotation.dump()}: ${annotation.errors.allErrors.dump()}"
            }

            if (!RevisionAnnotation.create(revision, annotation)) {
                log.error "Validation errors while saving annotation {} for revision {}: {}",
                    annotation.dump(), revision.id, revision.errors.allErrors
            }
        } catch (HibernateOptimisticLockingFailureException lfe) {
            log.error "Exception thrown while saving stale annotation {} or revision {}.",
                    annotation.dump(), revision.dump(), lfe
            annotation?.discard()
            revision.discard()
        } catch (Exception e) {
            log.error "Exception thrown while saving annotation {} for  revision {}: {}",
                    annotation.dump(), revision?.id, e
        }
        return annotation
    }
}
