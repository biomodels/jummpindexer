/**
 * Copyright (C) 2010-2017 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 **/





package net.biomodels.jummp.indexing

import net.biomodels.jummp.annotationstore.ResourceReference
import net.biomodels.jummp.indexing.miriam.MiriamRegistryService

/**
 * @author tnguyen@ebi.ac.uk on 26/04/17.
 */
class ReactomeDatabaseFetcher extends DatabaseBasedAnnoProcessor {

    ResourceReference getTermInformation(ResourceReference reference) {
        String id = reference.getAccession()
        String collType = reference.getDatatype()
        if (!cached.containsKey(id) && !loadFromDatabaseIntoCache(id, collType)) {
            String attributeName = "displayName"
            String server = "http://www.reactome.org/ContentService/data/query/"
            String ext = "${id}/${attributeName}"
            String url = "${server}${ext}"
            URL urlObject = new URL(url)
            URLConnection urlConnection = urlObject.openConnection()
            int responseCode = urlConnection.responseCode
            if (responseCode == 200) {
                String name
                try {
                    name = urlObject.getText()
                } catch (IOException e) {
                    logger.error("Reactome API threw an I/O exception for {}: {}", id, e.message)
                }
                if (name) {
                    reference.setName(name)
                } else {
                    logger.warn("Reactome API didn't return a label for {}", id)
                }
            }
            String collection = MiriamRegistryService.fetchCollectionNameByType(collType)
            reference.setCollectionName(collection)
            saveResource(reference)
        } else {
            Long cachedXrefId = cached.get(id)
            reference = ResourceReference.get(cachedXrefId)
            if (!reference) {
                logger.error("Could not obtain term information about xref {} ({})", id, collType)
            }
        }
        return reference
    }

    boolean supportsCollection(String collection) {
        return collection.equalsIgnoreCase("reactome")
    }
}
