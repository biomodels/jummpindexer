/**
 * Copyright (C) 2010-2016 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.orgin/licenses/agpl-3.0.html>.
 **/





package net.biomodels.jummp.indexing

import net.biomodels.jummp.indexing.omicsdiindexer.ProcessingIndexDataOmicsdi
import net.biomodels.jummp.indexing.solrindexer.ProcessingIndexDataSolr

/**
 * Simple class for interacting with a request's context.
 *
 * Effectively, this class parses an index file produced by Jummp and then provides
 * convenience methods for accessing the properties contained within the file.
 *
 * @author Mihai Glonț <mihai.glont@ebi.ac.uk>
 * @author Tung Nguyen <tung.nguyen@ebi.ac.uk>
 */

class IndexDataContext {
    private ProcessingIndexDataStrategy strategy

    IndexDataContext() {}

    void setProcessingIndexedDataStrategy(ProcessingIndexDataStrategy strategy) {
        this.strategy = strategy
    }

    void setProcessingIndexedDataStrategy(String strategyName) {
        switch (strategyName.toUpperCase()) {
            case "OMICSDI":
                this.strategy = new ProcessingIndexDataOmicsdi()
                break
            default:
                this.strategy = new ProcessingIndexDataSolr()
                break
        }
    }

    void indexDataBasedSearchStrategy(RequestContext ctx) {
        strategy.runIndexData(ctx)
    }
}
