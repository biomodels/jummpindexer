/**
 * Copyright (C) 2010-2017 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 **/





package net.biomodels.jummp.indexing

import net.biomodels.jummp.annotationstore.ResourceReference
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import java.util.concurrent.ConcurrentHashMap

/**
 * @author tnguyen@ebi.ac.uk on 02/05/17.
 */
abstract class ResourceReferenceResolver {
    private static final Logger logger = LoggerFactory.getLogger(ResourceReferenceResolver.class)
    ConcurrentHashMap<String, Long> cached = [:]

    void saveResource(ResourceReference reference) {
        try {
            if (reference.save()) {
                cached.put(reference.getAccession(), reference.id)
            } else {
                logger.error("Cannot persist xref {} with parents {} and synonyms {}: {}",
                    reference.uri, reference.parents?.collect {it.uri}, reference.synonyms,
                    reference.errors.allErrors)
            }
        }
        catch(Exception e) {
            logger.error("Error writing {} for {}, validation status {} to db:", reference,
                reference.uri, reference.errors.allErrors, e)
            reference.discard() // don't try to persist it!
        }
    }

    boolean loadFromDatabaseIntoCache(String id, String type) {
        ResourceReference ref = ResourceReference.findByAccessionAndDatatype(id, type)
        if (ref) {
            cached.put(id, ref.id)
            return true
        }
        return false
    }

    String toString () {
        this.getClass().name
    }

    protected void initialiseDataTypes() {}
}
