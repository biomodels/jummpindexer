/**
 * Copyright (C) 2010-2017 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 **/





package net.biomodels.jummp.indexing

import net.biomodels.jummp.annotationstore.ResourceReference
import net.biomodels.jummp.indexing.miriam.MiriamRegistryService
import org.json.JSONObject

/**
 * @author tnguyen@ebi.ac.uk on 24/04/17.
 */
class EnsemblDatabaseFetcher extends DatabaseBasedAnnoProcessor {
    final String server = "http://rest.ensembl.org/lookup/id/"

    ResourceReference getTermInformation(ResourceReference reference) {
        String id = reference.getAccession()
        String collType = reference.getDatatype()
        if (!cached.containsKey(id) && !loadFromDatabaseIntoCache(id, collType)) {
            URL url = new URL("${this.server}${id}")

            URLConnection connection = url.openConnection()
            HttpURLConnection httpConnection = (HttpURLConnection)connection

            httpConnection.setRequestProperty("Content-Type", "application/json")

            InputStream response = connection.getInputStream()
            int responseCode = httpConnection.getResponseCode()

            if(responseCode != 200) {
                throw new RuntimeException("Response code was not 200. Detected response was $responseCode")
            }

            String output
            Reader reader = null
            try {
                reader = new BufferedReader(new InputStreamReader(response, "UTF-8"))
                StringBuilder builder = new StringBuilder()
                char[] buffer = new char[8192]
                int read
                while ((read = reader.read(buffer, 0, buffer.length)) > 0) {
                    builder.append(buffer, 0, read)
                }
                output = builder.toString()
            }
            finally {
                if (reader != null) try {
                    reader.close()
                } catch (IOException e) {
                    logger.error("Failed to resolve Ensembl id {}: {}", id, e.message)
                }
            }
            if (output) {
                JSONObject json = new JSONObject(output)
                if (json.has("display_name")) {
                    String name = json.getString("display_name").toString()
                    if (name) {
                        reference.setName(name)
                    }
                }
            }
            String collection = MiriamRegistryService.fetchCollectionNameByType(collType)
            reference.setCollectionName(collection)
            saveResource(reference)
        } else {
            Long cachedXrefId = cached.get(id)
            reference = ResourceReference.get(cachedXrefId)
            if (!reference) {
                logger.error("Could not obtain term information about xref {} ({})", id, collType)
            }
        }

        return reference
    }

    boolean supportsCollection(String collection) {
        return collection.equalsIgnoreCase("ensembl")
    }
}
