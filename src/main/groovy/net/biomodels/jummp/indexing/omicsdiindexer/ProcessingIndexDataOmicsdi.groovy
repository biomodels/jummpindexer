/**
 * Copyright (C) 2010-2016 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.orgin/licenses/agpl-3.0.html>.
 **/





package net.biomodels.jummp.indexing.omicsdiindexer

import net.biomodels.jummp.indexing.IndexingPlan
import net.biomodels.jummp.indexing.IndexingStatus
import net.biomodels.jummp.indexing.ProcessingIndexDataStrategy
import net.biomodels.jummp.indexing.RequestContext
import net.biomodels.jummp.model.Model
import net.biomodels.jummp.model.Revision
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * @author Mihai Glonț <mihai.glont@ebi.ac.uk>
 * @author Tung Nguyen <tnguyen@ebi.ac.uk>
 */

class ProcessingIndexDataOmicsdi implements ProcessingIndexDataStrategy {
    /**
     * The class logger.
     */
    private static final Logger log = LoggerFactory.getLogger(ProcessingIndexDataOmicsdi.class)
    private static final boolean IS_DEBUG_ENABLED = log.isDebugEnabled()
    private static final boolean IS_INFO_ENABLED = log.isInfoEnabled()
    private static final boolean IS_ERROR_ENABLED = log.isErrorEnabled()

    ProcessingIndexDataOmicsdi() {
        if (IS_INFO_ENABLED) {
            log.info("OmicsDI indexing")
        }
    }

    void runIndexData(RequestContext ctx) {
        if (IS_INFO_ENABLED) {
            log.info "Beginning to index data for OmicsDI..."
        }
        Map<String, String> data = ctx.partialData
        IndexingPlan.withTransaction {
            Model model = Model.findById(data["model_id"])
            Revision revision = Revision.get(data["revision_id"])
            // set scheduledIndexing for this indexing plan at midnight
            Calendar calendar = Calendar.getInstance()
            calendar.setTime(new Date())
            calendar.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH) + 1)
            calendar.set(Calendar.HOUR_OF_DAY, 0)
            calendar.set(Calendar.MINUTE, 0)
            calendar.set(Calendar.SECOND, 0)
	        calendar.set(Calendar.MILLISECOND, 0)
            IndexingPlan plan = IndexingPlan.findByModel(model)
            if (plan) {
                plan.revision = revision
                plan.scheduledIndexing = calendar.getTime()
                plan.status = IndexingStatus.GENERATING
                if (IS_INFO_ENABLED) {
                    log.info "Updating the existing indexing plan."
                }
            } else {
                plan = new IndexingPlan(model: model, revision: revision,
                    scheduledIndexing: calendar.getTime(), status: IndexingStatus.GENERATING)
                if (IS_INFO_ENABLED) {
                    log.info "Creating a new indexing plan."
                }
            }
            if (plan.save(flush: true)) {
                if (IS_INFO_ENABLED) {
                    log.info("""\
Saved the indexing plan with id: $plan.id for the revision $revision.id of the model $model.submissionId into database successfully.""")
                }
            } else {
                if (IS_DEBUG_ENABLED) {
                    log.debug("""\
Failed to try saving the indexing plan with id: $plan.id for the revision $revision.id of the model $model.submissionId into database.""")
                }
                if (IS_ERROR_ENABLED) {
                    log.error("""\
Unable to save scheduled plan with id: $plan.id into database while indexing the revision $revision.id of the model $model.submissionId""")
                }
            }
            if (IS_INFO_ENABLED) {
                log.info "Ending to index data for OmicsDI..."
            }
        }
    }
}
