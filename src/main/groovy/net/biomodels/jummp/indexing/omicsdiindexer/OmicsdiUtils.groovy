/**
 * Copyright (C) 2010-2019 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.orgin/licenses/agpl-3.0.html>.
 **/





package net.biomodels.jummp.indexing.omicsdiindexer

import net.biomodels.jummp.indexing.AnnotationReferenceResolver

/**
 * Collecting util functions being used for OmicsDI export
 *
 * This class aims at gathering useful/helper functions being used in exporting OmicsDI XML.
 *
 * @author Mihai Glonț <mihai.glont@ebi.ac.uk>
 * @author Tung Nguyen <tnguyen@ebi.ac.uk>
 */
class OmicsdiUtils {
    static final AnnotationReferenceResolver resolver = new AnnotationReferenceResolver()
    static final Map<String, String> supportedDiseaseOntologies =
        ["doid": "Human Disease Ontology",
         "icd": "International Classification of Diseases",
         "ido": "Infectious Disease Ontology",
         "omim": "Online Mendelian Inheritance in Man"
        ]
    static final Map<String, String> supportedModellingApproaches =
        ["MAMO_0000046" : "Ordinary differential equation (ODE) model",
         "MAMO_0000041" : "Bayesian model",
         "MAMO_0000030" : "Logical model",
         "MAMO_0000025" : "Petri net model",
         "MAMO_0000024" : "Agent-based model",
         "MAMO_0000022" : "Rule-based model",
         "MAMO_0000009" : "Constraint-based model"
        ]

    static String tokenise(String name) {
        name?.replaceAll('[-_]', ' ')
    }

    /*
     * Converts a timestamp to an ISO8601 date (e.g. 2001-12-31)
     *
     * @param date the date to format
     *
     * @return the formatted date
     */
    static String formatDateForOmicsDI(Date date) {
        date?.format "yyyy-MM-dd"
    }
}
