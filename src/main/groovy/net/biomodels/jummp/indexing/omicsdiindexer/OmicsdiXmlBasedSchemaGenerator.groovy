/**
 * Copyright (C) 2010-2016 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.orgin/licenses/agpl-3.0.html>.
 **/





package net.biomodels.jummp.indexing.omicsdiindexer

import groovy.xml.MarkupBuilder
import groovyx.gpars.GParsPool
import net.biomodels.jummp.core.model.ModelState
import net.biomodels.jummp.indexing.RequestContext
import net.biomodels.jummp.model.Model
import net.biomodels.jummp.model.Revision
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import java.util.concurrent.LinkedBlockingQueue

/**
 * Generator of handling methods working with OmicsDI entries.
 *
 * This class is the mean of managing generation, creation and persistence OmicsDI entries.
 *
 * @author Mihai Glonț <mihai.glont@ebi.ac.uk>
 * @author Tung Nguyen <tnguyen@ebi.ac.uk>
 */

class OmicsdiXmlBasedSchemaGenerator {
    static final Logger logger = LoggerFactory.getLogger(OmicsdiXmlBasedSchemaGenerator.class)
    private static final boolean IS_INFO_ENABLED = logger.isInfoEnabled()

    /**
     * Pick up revisions which should be exported to OmicsDI.
     *
     * This method will pick the latest published revision of each model if it is ready,
     * the add it to the list of interest revisions.
     *
     * @param models The list of being considered models.
     * @return a list of involved revisions.
     */
    //TODO switch from List to Set
    List<Revision> selectModelRevisionsForExport(List<Model> models) {
        if (IS_INFO_ENABLED) {
            logger.info "Selecting the published revisions that will be exported for OmicsDI."
        }
        List<Revision> revisions = []
        /* this closure create a comparator to sort revisions on revision number in descending */
        def cmp = { r1, r2 ->
            r2.revisionNumber <=> r1.revisionNumber
        } as Comparator<Revision>
        for (Model m: models) {
            Set<Revision> nominatedRevisions = new TreeSet<>(cmp)
            nominatedRevisions.addAll(m.revisions.findAll { it.state == ModelState.PUBLISHED })
            if (!nominatedRevisions?.empty) {
                logger.info("Model ${m.id} has published revision ${nominatedRevisions.first()}")
            } else {
                logger.warn("No published revisions for model $m.id")
                // deal with private models
                nominatedRevisions.addAll(m.revisions)
            }
            revisions << nominatedRevisions.first()
        }
        return revisions
    }

    List<OmicsdiDataSetEntry> convertToOmicsdiDataSetEntry(List<Revision> revisions, RequestContext ctx) {
        logger.info("Converting revisions to OmicsDI entries...")
        def entries = new LinkedBlockingQueue<OmicsdiDataSetEntry>(revisions.size())

        GParsPool.withPool {
            revisions.eachParallel { Revision detachedRevision ->
                Model.withTransaction {
                    Revision r = Revision.get(detachedRevision.id)
                    Model m = r.model
                    String entryId = m.publicationId ?: m.submissionId
                    logger.info("...Processing revision ${r.id} of model ${entryId}")
                    OmicsdiDataSetEntryBuilder builder
                    builder = new OmicsdiDataSetEntryBuilder()
                                .setRequestContext(ctx)
                                .setModel(m)
                                .setRevision(r)
                                .setFieldsInMandatorySection()
                                .setOmicsFeaturedFields()
                    if (r.state == ModelState.PUBLISHED) {
                        builder.tokeniseName()
                            .setModelFields()
                            .setSubmitterInfo()
                            .setPublicationDetails()
                            .setQCInfo()
                            .setOmicsDiFieldsDerivedFromAnnotations()
                    }
                    OmicsdiDataSetEntry e = builder.build()
                    entries.add(e)
                }
            }
        }
        return entries.toList()
    }

    /**
     * Fetch OmicsDI knowledge from the database.
     *
     * This method will get OmicsDI knowledge of each model in the database.
     * @param  ctx      a RequestContext instance
     * @return a list   List of OmicsDI entries representing each model's omics knowledge.
     */
    List<OmicsdiDataSetEntry> fetchOmicsdiDataSetEntriesFromDatabase(RequestContext ctx) {
        List<OmicsdiDataSetEntry> entries = new ArrayList<OmicsdiDataSetEntry>()
        Long count = Model.withTransaction {
            Model.count()
        }
        Integer fetchSize = 100
        Integer times = Math.ceil((double)count/fetchSize)
        Long offset = 0
        Long total = 0
        List models = []
        String query = "from Model m where m.deleted = false"
        for (int index = 1; index <= times; ++index) {
            StringBuilder modelIDs = new StringBuilder()
            Model.withTransaction {
                models = Model.executeQuery(query, [offset: offset, max: fetchSize, readOnly: true])
                /**
                 * We didn't customise the default lazy loading behaviour on the relationship
                 * of Model and Revision. Accordingly GORM won't fetch revisions associated with
                 * models after running the above query. The following instruction is trying to load
                 * the revisions of a given model in expensive way by using dynamic finders.
                 * We might use fetch: [revisions: "eager"] with list method but it won't help
                 * much because GORM use left join by default. Thus the returned result is
                 * fewer than we are expecting.
                 */
                for (Model model: models) {
                    model.revisions = Revision.findAllByModel(model)
                    modelIDs.append(model.id)
                    if (index < times) {
                        modelIDs.append(',')
                    }
                }
            }

            logger.info("Batch $index: ${modelIDs.toString()}")
            if (models.size() > 0) {
                // sequential
                List<Revision> revisions = selectModelRevisionsForExport(models)
                logger.info("There are ${revisions.size()} revisions selected to export.")
                if (revisions.size() > 0) {
                    // parallel
                    List result = convertToOmicsdiDataSetEntry(revisions, ctx)

                    synchronized (this) {
                        entries.addAll(result)
                        offset += fetchSize
                        total += models.size()
                    }
                }
            }
        }
        logger.info("Total: ${total.toString()} <==> Count: ${count.toString()}")
        return entries
    }

    /**
     * Building a string of XML from the list of @OmicsdiDataSetEntry entries
     *
     * This methods builds a string of XML tags. The string represents the content of XML file
     * to be generated from OmicsDI entries.
     *
     * @param modelEntries a list of OmicsDI entries
     * @return a string representing XML format of OmicsDI entries
     */
    String buildStringOmicsdiSchemaXml(List<OmicsdiDataSetEntry> modelEntries) {
        logger.info("Building the string object representing OmicsDI XML content.")
        def stringWriter = new StringWriter()
        def markupBuilder = new MarkupBuilder(stringWriter)
        markupBuilder.setDoubleQuotes(true)
        markupBuilder.mkp.xmlDeclaration(version: "1.0", encoding: "utf-8")

        String _name = "BioModels"//grailsApplication.config.jummp.metadata.officialDatabaseName
        String _description = """\
BioModels is a repository of mathematical models of biological processes.
            Models described from literature are manually curated and enriched with cross-references.
            """
        //grailsApplication.config.jummp.metadata.officialDatabaseDescription
        byte _releaseVersion = 1
        Date _releaseDate = new Date()
        int _entryCount = modelEntries.size() ?: 0
        markupBuilder.database {
            // add the principal information of database into MarkupBuilder object
            name(_name)
            description(_description)
            release(_releaseVersion)
            release_date(_releaseDate)
            entry_count(_entryCount)
            // add each model into MarkupBuilder object
            entries {
                setOmitEmptyAttributes(true)
                setOmitNullAttributes(true)
                for (OmicsdiDataSetEntry e: modelEntries) {
                    entry(id: e.id) {
                        name(e.name)
                        if (e.description) {
                            description(e.description)
                        }
                        dates() {
                            if (e.dates.containsKey("submission")) {
                                if (e.dates.get("submission")) {
                                    date(type: "submission", value:
                                        OmicsdiUtils.formatDateForOmicsDI(e.dates.get("submission")))
                                }
                            }
                            if (e.dates.containsKey("publication")) {
                                if (e.dates.get("publication")) {
                                    date(type: "publication", value:
                                        OmicsdiUtils.formatDateForOmicsDI(e.dates.get("publication")))
                                }
                            }

                            if (e.dates.containsKey("modification")) {
                                if (e.dates.get("modification")) {
                                    date(type: "last_modification", value:
                                        OmicsdiUtils.formatDateForOmicsDI(e.dates.get("modification")))
                                }
                            }
                        }

                        additional_fields() {
                            if (e.submitterName) {
                                field(name: "submitter", e.submitterName)
                            }
                            if (e.submitterMail) {
                                field(name: "submitter_mail", e.submitterMail)
                            }
                            if (e.submitterAffiliation) {
                                field(name: "submitter_affiliation", e.submitterAffiliation)
                            }
                            if (e.repositoryName) {
                                field(name: "repository", e.repositoryName)
                            }
                            if (e.fullDataSetLink) {
                                field(name: "full_dataset_link", e.fullDataSetLink)
                            }
                            if (e.publication) {
                                field(name: "publication", e.publication)
                            }
                            if (e.diseaseNames) {
                                e.diseaseNames.each {
                                    field(name: "disease", it)
                                }
                            }
                            for (def f: e.flags) {
                                field(name: "modelFlag", f)
                            }
                            e.modellingApproaches?.each {
                                if (it)
                                    field(name: "modellingApproach", it)
                            }
                            if (e.omicsType) {
                                field(name: "omics_type", e.omicsType)
                            }
                            if (e.dataProtocol) {
                                field(name: "data_protocol", e.dataProtocol)
                            }
                            if (e.sampleProtocol) {
                                field(name: "sample_protocol", e.sampleProtocol)
                            }
                            if (e.technologyType) {
                                field(name: "technology_type", e.technologyType)
                            }
                            if (e.modelFormat) {
                                field(name: "modelFormat", e.modelFormat)
                            }
                            if (e.submissionId) {
                                field(name: "submissionId", e.submissionId)
                            }
                            if (e.publicationId) {
                                field(name: "publicationId", e.publicationId)
                            }
                            if (e.publicationYear) {
                                field(name: "publication_year", e.publicationYear)
                            }
                            if (e.levelVersion) {
                                field(name: "levelVersion", e.levelVersion)
                            }
                            if (e.validationStatus) {
                                field(name: "validationStatus", e.validationStatus)
                            }
                            if (e.curationStatus) {
                                field(name: "curationStatus", e.curationStatus)
                            }
                            if (e.certificationComment) {
                                field(name: "certificationComment", e.certificationComment)
                            }
                            if (e.elementName) {
                                field(name: "elementName", e.elementName)
                            }
                            if (e.elementId) {
                                field(name: "elementId", e.elementId)
                            }
                            if (e.elementDescription) {
                                field(name: "elementDescription", e.elementDescription)
                            }
                            if (e.sbmlSBOTerm) {
                                field(name: "sbmlSBOTerm", e.sbmlSBOTerm)
                            }
                            if (e.curators) {
                                field(name: "curators", e.curators)
                            }
                            if (e.isPrivate) {
                                field(name: "isPrivate", e.isPrivate)
                            }
                            StringBuilder xrefsNotUsedForIsDerivedFromAnno = e.nonIsDerivedFromBioModelsAnnotations?.inject(
                                    new StringBuilder(), { StringBuilder result, Map.Entry entry ->
                                result.append("${entry.key} ${entry.value} ")
                            })
                            if (xrefsNotUsedForIsDerivedFromAnno) {
                                def trimmed = xrefsNotUsedForIsDerivedFromAnno.subSequence(0,
                                        xrefsNotUsedForIsDerivedFromAnno.length() - 1)
                                field(name: "non_derived_xrefs", trimmed)
                            }

                            if (e.authors) {
                                def allAuthors = e.authors.join(', ')
                                String firstAuthor = e.authors.first()
                                field(name: "publication_authors", allAuthors)
                                field(name: "first_author", firstAuthor)
                            }
                            if (e.tokenisedName) {
                                field(name: "tokenised_name", e.tokenisedName)
                            }
                            if (e.derivations) {
                                field(name: "derivations", e.derivations)
                            }
                            if (e.pharmmlTherapeuticArea) {
                                field(name: "pharmmlTherapeuticArea", e.pharmmlTherapeuticArea)
                            }
                            if (e.pharmmlModellingContextDescription) {
                                field(name: "pharmmlModellingContextDescription", e.pharmmlModellingContextDescription)
                            }
                            if (e.pharmmlLongTechnicalDescription) {
                                field(name: "pharmmlLongTechnicalDescription", e.pharmmlLongTechnicalDescription)
                            }
                            if (e.pharmmlShortDescription) {
                                field(name: "pharmmlShortDescription", e.pharmmlShortDescription)
                            }
                            if (e.pharmmlPublicationSource) {
                                field(name: "pharmmlPublicationSource", e.pharmmlPublicationSource)
                            }
                            if (e.pharmmlImplementationConformsToLiterature) {
                                field(name: "pharmmlImplementationConformsToLiterature", e.pharmmlImplementationConformsToLiterature)
                            }
                            if (e.pharmmlImplementationDiscrepancies) {
                                field(name: "pharmmlImplementationDiscrepancies", e.pharmmlImplementationDiscrepancies)
                            }
                            if (e.pharmmlModelDevelopmentContext) {
                                field(name: "pharmmlModelDevelopmentContext", e.pharmmlModelDevelopmentContext)
                            }
                            if (e.pharmmlCodeFromLiterature) {
                                field(name: "pharmmlCodeFromLiterature", e.pharmmlCodeFromLiterature)
                            }
                            if (e.pharmmlResearchStage) {
                                field(name: "pharmmlResearchStage", e.pharmmlResearchStage)
                            }
                            if (e.pharmmlTasks) {
                                field(name: "pharmmlTasks", e.pharmmlTasks)
                            }
                            if (e.pharmmlTypeOfData) {
                                field(name: "pharmmlTypeOfData", e.pharmmlTypeOfData)
                            }
                        }
                        if (!e.crossReferences?.isEmpty()) {
                            cross_references() {
                                for (xref in e.crossReferences) {
                                    ref(dbkey: xref.key, dbname: xref.value)
                                }
                            }
                        }
                    }
                }
            }
        }
        def content = stringWriter.toString()
        return content
    }

    /**
     * Build an OmicsDI XML based schema file
     *
     * This method aims to build an XML file. Its content is the string of omics knowledge
     * under XML format.
     *
     * @param filePath a string representing the file path
     * @param fileName a string representing the file name
     * @param entries a list of OmicsDI entries
     * @return true/false
     */
    boolean buildFileOmicsdiSchemaXml(String filePath, String fileName, List<OmicsdiDataSetEntry> entries) {
        if (filePath.charAt(filePath.length() - 1) != File.separatorChar) {
            filePath += File.separatorChar
        }
        def fileWriter = new FileWriter("${filePath}${fileName}")
        String content = buildStringOmicsdiSchemaXml(entries)
        logger.info("Building XML file containing OmicsDI entries.")
        fileWriter.write(content)
        if (fileWriter != null && IS_INFO_ENABLED) {
            logger.info("Saved the XML file successfully.")
        }
        fileWriter.close()
        File file = new File("${filePath}${fileName}")
        return file.exists()
    }

    /**
     * Extract a sub list against the larger list of objects replied on the offset position and
     * step size (i.e. the number of elements beginning from the offset position forwards.
     *
     * @param entries   List of OmicsdiDataSetEntry instances
     * @param offset    a positive integer denoting the starting point where we begin partitioning elements
     * @param batchSize a positive integer denoting the amount of elements which are extracted to the new list
     *                  since the offset point
     * @return a list   List of OmicsdiDataSetEntry instances
     */
    List<OmicsdiDataSetEntry> partition(List<OmicsdiDataSetEntry> entries, int offset, int batchSize) {
        List<OmicsdiDataSetEntry> dataSetEntries = []
        offset = offset < 0 ? 0 : offset
        int limit = offset + batchSize;
        if (limit > entries.size()) {
            limit = entries.size()
        }
        for (int index = offset; index < limit; index++) {
            dataSetEntries.add(entries.get(index));
        }
        return dataSetEntries
    }
}
