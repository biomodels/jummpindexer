package net.biomodels.jummp.indexing;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ListPartitionSupport {

    public static <T> List<List<T>> partition(List<T> list, int chunkSize) {
        if (chunkSize <= 0) throw new IllegalArgumentException("A positive chunk size is required");
        int length = list.size();
        if (chunkSize >= length) return Collections.singletonList(list);

        final int chunkCount = length / chunkSize;
        final ArrayList<List<T>> result = new ArrayList<>();
        for (int i = 0; i < chunkCount; ++i) {
            List<T> chunk = list.subList(i * chunkSize, (i + 1) * chunkSize);
            result.add(chunk);
        }

        final int remainder = length % chunkSize;
        if (remainder != 0) {
            result.add(list.subList(chunkCount * chunkSize, length));
        }

        return result;
    }

    public static <T> List<List<T>> partitionUsingStreams(List<T> list, int chunkSize) {
        int listSize = list.size();
        return IntStream.range(0, (listSize - 1) / chunkSize + 1)
            .mapToObj(i -> list.subList(i *= chunkSize,
                listSize - chunkSize >= i ? i + chunkSize : listSize))
            .collect(Collectors.toList());
    }
}
