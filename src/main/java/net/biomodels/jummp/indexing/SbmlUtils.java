package net.biomodels.jummp.indexing;

import org.sbml.jsbml.CVTerm;
import org.sbml.jsbml.SBase;

public final class SbmlUtils {

    static SBase getSBaseForCVTerm(CVTerm term) {
        return (SBase) term.getParent().getParent();
    }
}
