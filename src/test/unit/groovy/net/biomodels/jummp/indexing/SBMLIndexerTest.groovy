/**
 * Copyright (C) 2010-2015 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 **/





package net.biomodels.jummp.indexing

import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.sbml.jsbml.Model
import org.sbml.jsbml.SBMLDocument
import org.sbml.jsbml.SBMLReader

import static org.junit.Assert.assertEquals
import static org.junit.Assert.assertNotNull

/**
 * @author Mihai Glonț <mihai.glont@ebi.ac.uk>
 */
@RunWith(JUnit4.class)
class SBMLIndexerTest {
    @Test
    void testAllExtractedSbmlRulesIncludeMetaId() {
        def is = this.getClass().classLoader.getResourceAsStream(
            "models/sbml-errors/BIOMD0000000114/BIOMD0000000114.xml")
        assertNotNull(is)
        SBMLDocument d = new SBMLReader().readSBMLFromStream(is)
        assertNotNull(d)
        Model m = d.getModel()
        assertNotNull(m)
        def indexer = new SBMLIndexer()
        List<Map> rules = indexer.getRules(m)
        int rulesWithMetaId = rules.collect { it.metaId }.size()
        int ruleCount = rules.size()
        assertEquals ruleCount, rulesWithMetaId
    }
}
