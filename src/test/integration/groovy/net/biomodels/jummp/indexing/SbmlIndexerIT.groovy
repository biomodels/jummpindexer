package net.biomodels.jummp.indexing

import net.biomodels.jummp.annotationstore.ElementAnnotation
import net.biomodels.jummp.annotationstore.RevisionAnnotation
import net.biomodels.jummp.model.ModelElementType
import net.biomodels.jummp.model.ModelFormat
import net.biomodels.jummp.model.Revision
import net.biomodels.jummp.plugins.security.User
import org.junit.After
import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.sbml.jsbml.AbstractSBase
import org.sbml.jsbml.ListOf
import org.sbml.jsbml.Model
import org.sbml.jsbml.SBMLReader
import org.sbml.jsbml.SBase
import org.springframework.transaction.annotation.Transactional

import static org.junit.Assert.assertEquals
import static org.junit.Assert.assertFalse
import static org.junit.Assert.assertFalse
import static org.junit.Assert.assertNotEquals
import static org.junit.Assert.assertNotNull
import static org.junit.Assert.assertNotNull
import static org.junit.Assert.assertTrue

/**
 * Integration tests for SBML indexer.
 */
@RunWith(JUnit4.class)
@Transactional
class SbmlIndexerIT {

    @After
    void tearDown() {
        // close any sessions opened by TestUtils.buildMockRequestContext
        GormUtil.closeSession()
    }

    @Test
    void testAnnotationExtractionForSbmlModels() {
        File json = new File("src/test/resources/models/sbml-anno/indexData.json")
        // the format identifier and version here should match the values from the JSON file
        def sbml = new ModelFormat(name: "SBML", identifier: "SBML", formatVersion: "L2V4")
        RequestContext ctx = TestUtils.buildMockRequestContext(json, sbml)
        SBMLIndexer indexer = (SBMLIndexer) ModelIndexerFactory.createIndexer(ctx)
        GormUtil.openSession()

        indexer.extractFileContent(ctx)

        String file = "src/test/resources/models/sbml-anno/BIOMD0000000272.xml"
        Model m = new SBMLReader().readSBML(file).getSBMLDocument()?.model

        def annoInspector = new SbmlAnnotationInspector()
        annoInspector.visit(m)
        int annoCount1 = annoInspector.getAnnotationCount()

        def indexingInspector = new SbmlIndexerAnnotationVisitor(indexer)
        indexingInspector.visit(m)
        int annoCount2 = indexingInspector.getAnnotationCount()
        assertEquals(annoCount1, annoCount2)

        for (ELEM_TYPE type: ELEM_TYPE.values()) {
            def fileAnnotations = annoInspector.getAnnotationsForElementType(type)
            def indexedAnnotations = indexingInspector.getAnnotationsForElementType(type)
            assertEquals "not all $type annotations got indexed", fileAnnotations, indexedAnnotations
        }
    }

    @Test
    @Ignore
    void "testAnnotationExtractionForRecon2.1"() {
        String jsonPath = "src/test/resources/models/sbml-anno/recon2-1.json"
        def sbmlL2V4 = new ModelFormat(name: "SBML", identifier: "SBML", formatVersion: "L2V4")
        RequestContext ctx = TestUtils.buildMockRequestContext(new File(jsonPath), sbmlL2V4)
        SBMLIndexer indexer = (SBMLIndexer) ModelIndexerFactory.createIndexer(ctx)

        indexer.extractFileContent(ctx)

        Integer annoCount = RevisionAnnotation.withTransaction {
            ElementAnnotation.count()
        }
        assertEquals(231018, annoCount)
    }

    @Test
    void "testModelsWithoutIdentifiers.OrgCrossReferences"() {
        String jsonPath = "src/test/resources/models/sbml-anno/BIOMD0000000084.json"
        def sbmlFormat = new ModelFormat(name: "SBML", identifier: "SBML", formatVersion: "L2V1")
        RequestContext ctx = TestUtils.buildMockRequestContext(new File(jsonPath), sbmlFormat)
        SBMLIndexer indexer = (SBMLIndexer) ModelIndexerFactory.createIndexer(ctx)

        indexer.extractFileContent(ctx)
        RevisionAnnotation.withTransaction {
            def annotations = RevisionAnnotation.getAll()
            assertEquals(29, annotations.size())
        }
    }

    @Test
    void indexingOfMultipleRevisionsForTheSameModel() {
        String jsonPath = "src/test/resources/models/sbml-anno/BIOMD0000000084.json"
        def sbmlFormat = new ModelFormat(name: "SBML", identifier: "SBML", formatVersion: "L2V1")
        RequestContext ctx = TestUtils.buildMockRequestContext(new File(jsonPath), sbmlFormat)
        SBMLIndexer indexer = (SBMLIndexer) ModelIndexerFactory.createIndexer(ctx)

        indexer.extractFileContent(ctx)
        RevisionAnnotation.withTransaction {
            def annotations = RevisionAnnotation.getAll()
            assertEquals(29, annotations.size())
        }

        RevisionAnnotation.withTransaction {
            def model = net.biomodels.jummp.model.Model.first()
            def revision = Revision.findByModelAndRevisionNumber(model, 2)
            if (!revision) {
                def format = ModelFormat.first()
                revision = new Revision(model: model, vcsId: "2", revisionNumber: 2,
                    owner: User.first(), minorRevision: false,
                    name: "interesting model v2", uploadDate: new Date(), format: format)
                model.revisions << revision
                assertNotNull(model.save(flush: true))
                assertFalse(model.hasErrors())

                assertTrue(revision.validate())
                assertNotNull(revision.save(flush: true))
                assertFalse(revision.hasErrors())
            }
            ctx.partialData.revision_id = revision.id
            assertEquals ctx.partialData.model_id, model.id
            assertEquals 2, model.revisions.size()
        }

        indexer.extractFileContent(ctx)
        RevisionAnnotation.withNewSession {
            def annotations = RevisionAnnotation.getAll()
            assertEquals(58, annotations.size())
        }
    }

    @Test
    void testL3V1CoreElementAnnotationsArePersistedCorrectly() {
        File json = new File("src/test/resources/models/sbml-anno/indexData.json")
        // the format identifier and version here should match the values from the JSON file
        def sbml = new ModelFormat(name: "SBML", identifier: "SBML", formatVersion: "L2V4")
        RequestContext ctx = TestUtils.buildMockRequestContext(json, sbml)
        SBMLIndexer indexer = (SBMLIndexer) ModelIndexerFactory.createIndexer(ctx)
        indexer.extractFileContent(ctx)
        Revision.withTransaction {
            def revisionAnnotations = ElementAnnotation.list()
            assertEquals(1, Revision.count())
            assertEquals(27, revisionAnnotations.size())
            [
                (ELEM_TYPE.MODEL): [ name: 'model', count: 8 ],
                (ELEM_TYPE.COMPARTMENT): [ name: 'compartment', count: 3 ],
                (ELEM_TYPE.REACTION): [ name: 'reaction', count: 9 ],
                (ELEM_TYPE.SPECIES): [ name: 'species', count: 7 ]
            ].each { t, props ->
                ModelElementType elemType = ModelElementType.findByName(props.name)
                long count = ElementAnnotation.countByModelElementType(elemType)
                assertEquals("Didn't persist all annotations for $t", props.count, count)
            }

        }
    }

    @Test
    void testSpeciesAnnotationPersistence() {
        String jsonFileName = "src/test/resources/models/sbml-errors/BIOMD0000000004/indexData.json"
        File json = new File(jsonFileName)
        // the format identifier and version here should match the values from the JSON file
        def sbml = new ModelFormat(name: "SBML", identifier: "SBML", formatVersion: "L2V4")
        RequestContext ctx = TestUtils.buildMockRequestContext(json, sbml)
        SBMLIndexer indexer = (SBMLIndexer) ModelIndexerFactory.createIndexer(ctx)
        indexer.extractFileContent(ctx)
        String sbmlFileName =  "src/test/resources/models/sbml-errors/BIOMD0000000004/BIOMD0000000004.xml"
        Model m = new SBMLReader().readSBML(sbmlFileName).getSBMLDocument()?.model

        List<Map> allSpecies = indexer.getAllSpecies(m)
        assertEquals(allSpecies.size(), m.getSpeciesCount())
        int expectedSpeciesAnnotationCount = allSpecies.findAll {
            // find all species with annotations
            it.annotation?.size() > 0
        }.collect {
            // for each species' annotation collection extract the list
            // of corresponding cross references
            it.annotation*.resources
        }.flatten().size() // flatten the resulting nested list of cross references
        ElementAnnotation.withTransaction {
            sbml = ModelFormat.findByIdentifierAndFormatVersion(sbml.identifier, sbml.formatVersion)
            assertEquals(expectedSpeciesAnnotationCount, ElementAnnotation.countByModelElementType(
                ModelElementType.findByModelFormatAndName(sbml, "species")
            ))
        }

        List<Map> allReactions = indexer.getReactions(m)
        println allReactions.size()
        /*
         * CHEBI:36080
         * P62988
         *
         */
    }
}

enum ELEM_TYPE {
    MODEL,
    COMPARTMENT,
    SPECIES,
    REACTION,
    PARAMETER,
    INIT_ASSIGN,
    FUNC_DEF,
    EVENT,
    RULE,
    CONSTRAINT
}

abstract class AnnoVisitor {
    protected final HashMap<ELEM_TYPE, List<String>> xrefMap = new HashMap()
    abstract void visit(Model model)

    Map<ELEM_TYPE, List<String>> getAnnotations() {
        xrefMap
    }

    Integer getAnnotationCount() {
        xrefMap.values()?.collect { it.size() }?.sum()
    }

    List getAnnotationsForElementType(ELEM_TYPE type) {
        assert type
        xrefMap.get(type)
    }

    protected List<String> getSBaseXrefs(List<? extends SBase> sbases) {
        sbases.collect { SBase elem -> getSBaseXrefs( elem) }?.flatten()
    }

    protected List<String> getSBaseXrefs(SBase sbase) {
        sbase?.getCVTerms()?.collect {it.getResources()}?.flatten()
    }
}

class SbmlAnnotationInspector extends AnnoVisitor {
    void visit(Model m) {
        def modelXrefs = getSBaseXrefs(m.getModel())
        xrefMap[ELEM_TYPE.MODEL] = modelXrefs

        // go through all known L3V1 core element types
        Map<ELEM_TYPE, List<? extends SBase>> mapping = [
            (ELEM_TYPE.COMPARTMENT) : m.getListOfCompartments(),
            (ELEM_TYPE.CONSTRAINT) : m.getListOfConstraints(),
            (ELEM_TYPE.EVENT) : m.getListOfEvents(),
            (ELEM_TYPE.FUNC_DEF) : m.getListOfFunctionDefinitions(),
            (ELEM_TYPE.INIT_ASSIGN) : m.getListOfInitialAssignments(),
            (ELEM_TYPE.PARAMETER) : m.getListOfParameters(),
            (ELEM_TYPE.REACTION) : m.getListOfReactions(),
            (ELEM_TYPE.RULE) : m.getListOfRules(),
            (ELEM_TYPE.SPECIES) : m.getListOfSpecies()
        ]
        mapping.each { ELEM_TYPE type, ListOf<? extends AbstractSBase> sbases ->
            List<String> xrefs = getSBaseXrefs(sbases)
            xrefMap[type] = xrefs ?: []
        }
    }
}

class SbmlIndexerAnnotationVisitor extends AnnoVisitor {
    private final SBMLIndexer indexer

    SbmlIndexerAnnotationVisitor(SBMLIndexer indexer) {
        this.indexer = indexer
    }

    void visit(Model m) {
        assert indexer && m
        def modelAnno = indexer.getAnnotations(m)
        def cmpAnno = indexer.getCompartments(m)
        def cnstrAnno = indexer.getConstraints(m)
        def evtAnno = indexer.getEvents(m)
        def fdAnno = indexer.getFunctionDefinitions(m)
        def iaAnno = indexer.getInitialAssignments(m)
        def pAnno = indexer.getParameters(m)
        def reacAnno = indexer.getReactions(m)
        def ruAnno = indexer.getRules(m)
        def sAnno = indexer.getAllSpecies(m)
        xrefMap[ELEM_TYPE.MODEL] = getAnnoFromIndexerOutput(modelAnno)
        xrefMap[ELEM_TYPE.COMPARTMENT] = getAnnoFromIndexerOutput(cmpAnno)
        xrefMap[ELEM_TYPE.CONSTRAINT] = getAnnoFromIndexerOutput(cnstrAnno)
        xrefMap[ELEM_TYPE.EVENT] = getAnnoFromIndexerOutput(evtAnno)
        xrefMap[ELEM_TYPE.FUNC_DEF] = getAnnoFromIndexerOutput(fdAnno)
        xrefMap[ELEM_TYPE.INIT_ASSIGN] = getAnnoFromIndexerOutput(iaAnno)
        xrefMap[ELEM_TYPE.PARAMETER] = getAnnoFromIndexerOutput(pAnno)
        xrefMap[ELEM_TYPE.REACTION] = getAnnoFromIndexerOutput(reacAnno)
        xrefMap[ELEM_TYPE.RULE] = getAnnoFromIndexerOutput(ruAnno)
        xrefMap[ELEM_TYPE.SPECIES] = getAnnoFromIndexerOutput(sAnno)
    }

    private List<String> getAnnoFromIndexerOutput(List<Map> sbases) {
        sbases.collect {
            def annos = it.annotation
            extractXrefsFromAnnotations(annos).flatten()
        }.flatten()
    }

    private List<String> extractXrefsFromAnnotations(List<Map> annotations) {
        annotations*.resources*.urn?.flatten()
    }
}
