package net.biomodels.jummp.indexing

import net.biomodels.jummp.model.ModelElementType
import net.biomodels.jummp.model.ModelFormat
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Utility class for creating model element types.
 *
 * Typically used at test runtime to persist the expected model element types for
 * a given format.
 *
 * @author Mihai Glonț <mihai.glont@ebi.ac.uk>
 */
final class ModelElementTypeUtils {
    private static final Logger logger = LoggerFactory.getLogger(getClass())

    public static final Set SBML_ELEMEMTS = Collections.unmodifiableSet([
            'model', 'compartment', 'constraint', 'event', 'functionDefinition',
            'initialAssignment', 'parameter', 'reaction', 'rule', 'species'
    ] as Set)
    public static final Set DEFAULT_ELEMENTS = Collections.unmodifiableSet(['model'] as Set)

    static boolean createForFormat(ModelFormat format) {
        assert format?.identifier && format?.formatVersion && format?.identifier
        switch (format.identifier) {
            case "SBML":
                return createForFormatAndElements(format, SBML_ELEMEMTS)
            default:
                return createForFormatAndElements(format, DEFAULT_ELEMENTS)
        }
    }

    static boolean createForFormatAndElements(ModelFormat format, Set<String> elements) {
        assert format?.identifier && format?.formatVersion && format?.identifier
        if (!elements) {
            return createForFormat(format)
        }

        boolean result = true
        for (String e: elements) {
            def elemType = ModelElementType.findOrSaveByModelFormatAndName(format, e)
            if (!elemType || elemType?.hasErrors()) {
                result = false
                logger.error("Could not persist element type $e")
            }  else {
                logger.info("Successfully created {} for {}", e, format?.identifier)
            }
        }
        result
    }
}
